<?php

/*
|----------------------------------------------------
|  Framework: setup of the custom functions
|----------------------------------------------------
*/

require_once("inc/cleanup.php"); // Cleanup of the admin area
require_once("inc/custom-theme.php"); // Adds custom features to the website
require_once("inc/disable-comments.php"); // Disables comments site-wide

define( 'DISALLOW_FILE_EDIT', true ); //Disable theme and plugin editors
//add_filter('jetpack_development_mode', '__return_true' ); // Sets Jetpack to dev mode
add_action( 'admin_menu', 'gp_remove_menus' ); // Remove menu's for Dev -> Production
add_action( 'admin_bar_menu', 'remove_some_nodes_from_admin_top_bar_menu', 999 ); // Remove customizer option from top bar
add_action( 'admin_menu', 'disable_customizer' ); // Remove customizer from wp-admin

/*************** CUSTOM PHP BELOW ***************/
function check_login() {
    if (!is_user_logged_in()) {
        auth_redirect();
    }
}
add_shortcode('check_login', 'check_login');

// Klantenrol

$result = add_role( 'klant', __('Klant' ),
  array(
    'read'              => false,
    'edit_posts'        => false,
    'edit_pages'        => false,
    'edit_others_posts' => false,
    'create_posts'      => false,
    'manage_categories' => false,
    'publish_posts'     => false,
    'edit_themes'       => false,
    'install_plugins'   => false,
    'update_plugin'     => false,
    'update_core'       => false
  )
);

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
  if (current_user_can('klant')) {
    show_admin_bar(false);
  }
}
