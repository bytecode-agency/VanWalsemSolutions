<?php

/*
|----------------------------------------------------
|  Framework: Hide metaboxes
|----------------------------------------------------
*/

function generate_remove_metaboxes()
{
    remove_action('add_meta_boxes', 'generate_add_layout_meta_box');
    remove_action('add_meta_boxes', 'generate_add_footer_widget_meta_box');
    //remove_action('add_meta_boxes', 'generate_add_de_meta_box');
    remove_action('add_meta_boxes', 'generate_add_page_builder_meta_box');
}
add_action( 'after_setup_theme','generate_remove_metaboxes' );

/*
|----------------------------------------------------
|  Page Builder: Disable generation of css files
|----------------------------------------------------
*/

function wbe_filter_widget_css( $css, $instance, $widget ){
}
add_filter('siteorigin_widgets_instance_css', 'wbe_filter_widget_css', 10, 3);

/*
|----------------------------------------------------
|  Page Builder: Add css to hide forbidden options
|----------------------------------------------------
*/

function admin_style() {
  wp_enqueue_style('admin-styles', get_stylesheet_directory_uri().'/css/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

/*
|------------------------------------------------------
|  GeneratePress: Disable collapse of secondary nav
|------------------------------------------------------
*/

add_action( 'wp_enqueue_scripts', 'generate_dequeue_secondary_nav_mobile', 999 );
function generate_dequeue_secondary_nav_mobile() {
   wp_dequeue_style( 'generate-secondary-nav-mobile' );
}

/*
|------------------------------------------------------
|  WordPress: Enqueue Custom CSS & JS
|------------------------------------------------------
*/

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
function add_theme_scripts() {
  wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/custom.js', array ( 'jquery' ), 1.0, true);
}

/*
|----------------------------------------------------
|  WordPress: Remove and/or Rename userrole(s)
|----------------------------------------------------
*/

// Remove Role: Subscriber / Abonnee
if( get_role('subscriber') ){
      remove_role( 'subscriber' );
}

// Remove Role: Author / Auteur
if( get_role('author') ){
      remove_role( 'author' );
}

// Remove Role: Contributor / Schrijver
if( get_role('contributor') ){
      remove_role( 'contributor' );
}

// Remove Role: BackWPup-beheerder
if( get_role('backwpup_admin') ){
      remove_role( 'backwpup_admin' );
}

// Remove Role: BackWPup taken-controle
if( get_role('backwpup_check') ){
      remove_role( 'backwpup_check' );
}

// Remove Role: BackWPup takenhulp
if( get_role('backwpup_helper') ){
      remove_role( 'backwpup_helper' );
}

// Remove Role: Translator
if( get_role('translator') ){
      remove_role( 'translator' );
}

// Rename userrole(s)
function change_role_name() {
    global $wp_roles;

    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    //You can list all currently available roles like this...
    //$roles = $wp_roles->get_names();
    //echo '<pre>',print_r($roles),'</pre>';

    //Default roles are: "administrator", "editor", "author", "contributor" or "subscriber"...
    $wp_roles->roles['administrator']['name'] = 'Webmaster';
    $wp_roles->role_names['administrator'] = 'Webmaster';

    $wp_roles->roles['editor']['name'] = 'Beheer';
    $wp_roles->role_names['editor'] = 'Beheer';

}
add_action('init', 'change_role_name');

// Give editor more privileges:
    //add caps to editor role
    $role = get_role("editor");
    //for woocommerce
    $role->add_cap("manage_woocommerce");
    $role->add_cap("view_woocommerce_reports");
    $role->add_cap("edit_product");
    $role->add_cap("read_product");
    $role->add_cap("delete_product");
    $role->add_cap("edit_products");
    $role->add_cap("edit_others_products");
    $role->add_cap("publish_products");
    $role->add_cap("read_private_products");
    $role->add_cap("delete_products");
    $role->add_cap("delete_private_products");
    $role->add_cap("delete_published_products");
    $role->add_cap("delete_others_products");
    $role->add_cap("edit_private_products");
    $role->add_cap("edit_published_products");
    $role->add_cap("manage_product_terms");
    $role->add_cap("edit_product_terms");
    $role->add_cap("delete_product_terms");
    $role->add_cap("assign_product_terms");
    $role->add_cap("edit_shop_order");
    $role->add_cap("read_shop_order");
    $role->add_cap("delete_shop_order");
    $role->add_cap("edit_shop_orders");
    $role->add_cap("edit_others_shop_orders");
    $role->add_cap("publish_shop_orders");
    $role->add_cap("read_private_shop_orders");
    $role->add_cap("delete_shop_orders");
    $role->add_cap("delete_private_shop_orders");
    $role->add_cap("delete_published_shop_orders");
    $role->add_cap("delete_others_shop_orders");
    $role->add_cap("edit_private_shop_orders");
    $role->add_cap("edit_published_shop_orders");
    $role->add_cap("manage_shop_order_terms");
    $role->add_cap("edit_shop_order_terms");
    $role->add_cap("delete_shop_order_terms");
    $role->add_cap("assign_shop_order_terms");
    $role->add_cap("edit_shop_coupon");
    $role->add_cap("read_shop_coupon");
    $role->add_cap("delete_shop_coupon");
    $role->add_cap("edit_shop_coupons");
    $role->add_cap("edit_others_shop_coupons");
    $role->add_cap("publish_shop_coupons");
    $role->add_cap("read_private_shop_coupons");
    $role->add_cap("delete_shop_coupons");
    $role->add_cap("delete_private_shop_coupons");
    $role->add_cap("delete_published_shop_coupons");
    $role->add_cap("delete_others_shop_coupons");
    $role->add_cap("edit_private_shop_coupons");
    $role->add_cap("edit_published_shop_coupons");
    $role->add_cap("manage_shop_coupon_terms");
    $role->add_cap("edit_shop_coupon_terms");
    $role->add_cap("delete_shop_coupon_terms");
    $role->add_cap("assign_shop_coupon_terms");
    $role->add_cap("edit_shop_webhook");
    $role->add_cap("read_shop_webhook");
    $role->add_cap("delete_shop_webhook");
    $role->add_cap("edit_shop_webhooks");
    $role->add_cap("edit_others_shop_webhooks");
    $role->add_cap("publish_shop_webhooks");
    $role->add_cap("read_private_shop_webhooks");
    $role->add_cap("delete_shop_webhooks");
    $role->add_cap("delete_private_shop_webhooks");
    $role->add_cap("delete_published_shop_webhooks");
    $role->add_cap("delete_others_shop_webhooks");
    $role->add_cap("edit_private_shop_webhooks");
    $role->add_cap("edit_published_shop_webhooks");
    $role->add_cap("manage_shop_webhook_terms");
    $role->add_cap("edit_shop_webhook_terms");
    $role->add_cap("delete_shop_webhook_terms");
    $role->add_cap("assign_shop_webhook_terms");

add_action( 'admin_menu', 'remove_menu_pages', 999);
function remove_menu_pages() {
  global $current_user;

  $user_roles = $current_user->roles;
  $user_role = array_shift($user_roles);
  if($user_role == "editor") {
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-settings');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
  }
}
// To reset default userroles remove above code and use:
/*
if ( !function_exists( 'populate_roles' ) ) {
  require_once( ABSPATH . 'wp-admin/includes/schema.php' );
}
populate_roles();
*/

/*
|------------------------------------------------------
|  WordPress: Change the Login page
|------------------------------------------------------
*/

//Change logo
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(/wp-content/themes/generatepress-child/logo.png);
            width: 320px;
            background-size: 160px;
			height: 100px;
        }
    </style>
<?php }

//Change error text
add_action('register_form', 'login_form_message');
function login_form_message() {
    echo '<p>Incorrent login, please try again</p>';
}

//Login style sheet
function my_login_stylesheet() {
    wp_enqueue_style( 'login-head', get_stylesheet_directory_uri().'/css/style-login.css', false );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

/*
|----------------------------------------------------
|  WordPress: More unique slugs for attachments
|----------------------------------------------------
*/

add_filter( 'wp_unique_post_slug', 'unique_post_slug', 10, 6 );
function unique_post_slug( $slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug ) {
  if ( 'attachment' == $post_type )
    $slug = $original_slug . uniqid( '-' );
  return $slug;
}

/*
|------------------------------------------------------
|  WordPress: Add searchform to secondary nav
|------------------------------------------------------
*/

// add_filter('wp_nav_menu_items','add_search_box', 10, 2);
function add_search_box($items, $args) {

	if( $args->theme_location == 'secondary')  {
        ob_start();
        get_search_form();
        $searchform = ob_get_contents();
        ob_end_clean();

        $items .= '<li class="search-item">' . $searchform . '</li>';
    }
    return $items;
}

/*
|------------------------------------------------------
|  WordPress: Remove pages from the WP-Admin area
|------------------------------------------------------
*/

//Remove menus
function gp_remove_menus(){
    //remove_menu_page( 'index.php' );                          //Dashboard
    remove_menu_page( 'jetpack' );                              //Jetpack
    //remove_menu_page( 'edit.php' );                           //Posts
    //remove_menu_page( 'upload.php' );                         //Media
    //remove_menu_page( 'edit.php?post_type=page' );            //Pages
    remove_menu_page( 'link-manager.php' );                     //Links
    remove_menu_page( 'edit-comments.php' );                    //Comments
    remove_submenu_page( 'themes.php', 'themes.php' );          //Themes selector
    //remove_menu_page( 'themes.php' );                         //Appearance complete
    remove_submenu_page( 'themes.php' , 'gp_hooks_settings' );  //GP Hooks
    remove_submenu_page( 'themes.php', 'generate-options' );    //GP Options
    //remove_menu_page( 'plugins.php' );                        //Plugins
    //remove_menu_page( 'users.php' );                          //Users
    remove_menu_page( 'tools.php' );                            //Tools
    //remove_menu_page( 'options-general.php' );                //Settings
    //remove_menu_page( 'admin.php?page=duplicator' );          //Duplicator
    //remove_menu_page( 'admin.php?page=elementor' );           //Elementor
	remove_menu_page( 'of-options-wizard' );                	//The7 theme options
    remove_submenu_page( 'plugins.php' , 'install-required-plugins' ); //The7 plugins
    remove_submenu_page( 'options-general.php' , 'disable_comments_settings' ); //Disable comments
}

//Disable customizer in top bar
function remove_some_nodes_from_admin_top_bar_menu( $wp_admin_bar ) {
    $wp_admin_bar->remove_menu( 'customize' );
}

//Disable customizer in wp-admin
function disable_customizer() {
    global $submenu;
    if ( isset( $submenu[ 'themes.php' ] ) ) {
        foreach ( $submenu[ 'themes.php' ] as $index => $menu_item ) {
            if ( in_array( 'Customize', $menu_item ) ) {
                unset( $submenu[ 'themes.php' ][ $index ] );
            }
        }
    }
}

/*
|------------------------------------------------------
|  WooCommerce:  Rename 'Add to cart'
|------------------------------------------------------
*/
add_filter('gettext', 'translate_add');
add_filter('ngettext', 'translate_add');
function translate_add($translated)
{
	$translated = str_ireplace('Add to cart', 'Order Now', $translated);
	return $translated;
}
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );    // 2.1 +

function woo_archive_custom_cart_button_text() {

        return __( 'In Winkelwagen', 'woocommerce' );

}

/*
|------------------------------------------------------
|  WooCommerce Detail: Title on top
|------------------------------------------------------
*/

//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
//add_action( 'woocommerce_before_single_product_summary', 'woocommerce_template_single_title', 5 );

/*
|------------------------------------------------------
|  WooCommerce Detail: Move price to bottom
|------------------------------------------------------
*/

//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );
//add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

/*
|------------------------------------------------------
|  WooCommerce Detail: Move sale flash to price
|------------------------------------------------------
*/

//remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
//add_action( 'woocommerce_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
